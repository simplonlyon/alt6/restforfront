package co.simplon.alt6.restforfront.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

import co.simplon.alt6.restforfront.entity.Dog;

@RestResource
@Repository
public interface DogRepository extends JpaRepository<Dog,Integer>{
    
}
