package co.simplon.alt6.restforfront;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

import co.simplon.alt6.restforfront.entity.Dog;

@SpringBootApplication
public class RestforfrontApplication implements RepositoryRestConfigurer {

	public static void main(String[] args) {
		SpringApplication.run(RestforfrontApplication.class, args);
	}

	@Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config, CorsRegistry cors) {
        config.exposeIdsFor(Dog.class);
		cors.addMapping("/*")
		.allowedOrigins("*")
		.allowedMethods("*");
    }

}
